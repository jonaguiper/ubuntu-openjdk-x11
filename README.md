# DOCKER UBUNTU-OPENJDK-X11

# PROPÓSITO
Crear una imagen oar que sea la base de otras que puedan crear aplicaciones desktop en contenedores. Esto fue creado apartir de la idea/repositorio: https://github.com/DrSnowbird/docker-project-template.
Al crear contenedores con esta imagen solo se ejecuta firefox para comprobar el correcto funcionamiento de la imagen/contenedor.

# Concepts
## Variables:
- Las variables de entorno utilizadas para la creación de la imagen están en docker-build.env
- Las variables de entorno utilizadas para la creación de un contenedor están en docker-run.env
- Los volumenes se crean segun lo que se establece en docker-run.env, en este caso por ejemplo con la sintaxys: #VOLUMES_LIST="data workspace"

## Ejecución
- Para construir la imagen solo hace falta ejecutar ./build.sh
- Para crear el contenedor y ejecutarlo solo hace falta ejecutar ./run.sh
- For Product usage, please ensure all aspects of security by enhancing source code and setup.
