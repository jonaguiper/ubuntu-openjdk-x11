FROM ubuntu:bionic

#MAINTAINER Jonaguiper "jonaguiper@gmail.com"

ENV DEBIAN_FRONTEND noninteractive

#### ---- Build Specification ----# Metadata params
ARG BUILD_DATE=${BUILD_DATE:-}
ARG VERSION=${BUILD_DATE:-}
ARG VCS_REF=${BUILD_DATE:-}

#### ---- Product Specifications ----
#ARG PRODUCT=${PRODUCT:-}
#ARG PRODUCT_VERSION=${PRODUCT_VERSION:-}
#ARG PRODUCT_DIR=${PRODUCT_DIR:-}
#ARG PRODUCT_EXE=${PRODUCT_EXE:-}
#ENV PRODUCT=${PRODUCT}
#ENV PRODUCT_VERSION=${PRODUCT_VERSION}
#ENV PRODUCT_DIR=${PRODUCT_DIR}
#ENV PRODUCT_EXE=${PRODUCT_EXE}

# Metadata
LABEL maintainer="jonaguiper@gmail.com" \
      org.label-schema.url="https://imagelayers.io" \
      org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.version=$VERSION \
      org.label-schema.vcs-url="https://github.com/microscaling/imagelayers-graph.git" \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.docker.dockerfile="/Dockerfile" \
      org.label-schema.description="Imagen base para crear contenedores de aplicaciones desktop" \
      org.label-schema.schema-version="1.0"

#########
ARG DISPLAY=${DISPLAY:-":0.0"}
ENV DISPLAY=${DISPLAY}

ARG USER_ID=${USER_ID:-1000}
ENV USER_ID=${USER_ID}

ARG GROUP_ID=${GROUP_ID:-1000}
ENV GROUP_ID=${GROUP_ID}

########################################
##### update ubuntu and Install 
########################################
RUN apt-get update && \
    apt-get install -y apt-utils && \
#    apt-get install -y apt-utils automake pkg-config libpcre3-dev zlib1g-dev liblzma-dev && \
    apt-get install -y curl net-tools && \
#   apt-get install -y curl net-tools build-essential software-properties-common bzip2 libbz2-dev  && \
    apt-get install -y xz-utils git wget unzip && \
#    apt-get install -y xz-utils git wget unzip vim && \
## ---- X11 ----
    apt-get install -y sudo xauth xorg openbox && \
    apt-get install -y libxext-dev libxrender-dev libxtst-dev firefox && \
    apt-get install -y apt-transport-https ca-certificates libcurl3-gnutls && \
#
    apt-get install -y packagekit-gtk3-module libcanberra-gtk3-module && \
#
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

    
ENV INSTALL_DIR=${INSTALL_DIR:-/usr}

###################################
#### Install Java 
###################################
#### ---------------------------------------------------------------
#### ---- Change below when upgrading version ----
#### ---------------------------------------------------------------
## https://download.oracle.com/otn-pub/java/jdk/8u201-b09/42970487e3af4f5aa5bca3f542482c60/jdk-8u201-linux-x64.tar.gz
ARG JAVA_MAJOR_VERSION=${JAVA_MAJOR_VERSION:-12}
ARG JAVA_UPDATE_VERSION=${JAVA_UPDATE_VERSION:-33}
#ARG JAVA_BUILD_NUMBER=${JAVA_BUILD_NUMBER:-33}
#ARG JAVA_DOWNLOAD_TOKEN=${JAVA_DOWNLOAD_TOKEN:-42970487e3af4f5aa5bca3f542482c60}

#### ---------------------------------------------------------------
#### ---- Don't change below unless you know what you are doing ----
#### ---------------------------------------------------------------
#ARG UPDATE_VERSION=${JAVA_MAJOR_VERSION}u${JAVA_UPDATE_VERSION}
#ARG BUILD_VERSION=b${JAVA_BUILD_NUMBER}

ENV JAVA_HOME_ACTUAL=${INSTALL_DIR}/jdk-${JAVA_MAJOR_VERSION}
#ENV JAVA_HOME_ACTUAL=${INSTALL_DIR}/jdk1.${JAVA_MAJOR_VERSION}.0_${JAVA_UPDATE_VERSION}
ENV JAVA_HOME=${INSTALL_DIR}/java

ENV PATH=$PATH:${JAVA_HOME}/bin

WORKDIR ${INSTALL_DIR}

#RUN echo "DESCARGANDO Y E INSTALANDO JAVA ${JAVA_MAJOR_VERSION}"
RUN curl -sL --retry 3 --insecure \
  #--header "Cookie: oraclelicense=accept-securebackup-cookie;" \
  "https://download.java.net/java/GA/jdk${JAVA_MAJOR_VERSION}/${JAVA_UPDATE_VERSION}/GPL/openjdk-${JAVA_MAJOR_VERSION}_linux-x64_bin.tar.gz" \
  #"http://download.oracle.com/otn-pub/java/jdk/${UPDATE_VERSION}-${BUILD_VERSION}/${JAVA_DOWNLOAD_TOKEN}/jdk-${UPDATE_VERSION}-linux-x64.tar.gz" \
  | gunzip \
  | tar x -C ${INSTALL_DIR}/ \
  && rm -rf openjdk-${JAVA_MAJOR_VERSION}_linux-x64_bin.tar.gz # ${JAVA_HOME_ACTUAL}/man \
  #&& rm -rf jdk-${UPDATE_VERSION}-linux-x64.tar.gz # ${JAVA_HOME_ACTUAL}/man
  && echo "DESCARGANDO Y E INSTALANDO JAVA ${JAVA_MAJOR_VERSION}"

############################
#### --- JAVA_HOME --- #####
############################
ENV JAVA_HOME=$INSTALL_DIR/java

###################################
#### Install Maven 3
###################################ENV MAVEN_VERSION 3.5.3
#ARG MAVEN_VERSION=${MAVEN_VERSION:-3.6.0}
#ENV MAVEN_VERSION=${MAVEN_VERSION}
#ENV MAVEN_HOME=/usr/apache-maven-${MAVEN_VERSION}
#ENV PATH=${PATH}:${MAVEN_HOME}/bin
#RUN curl -sL http://archive.apache.org/dist/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
#  | gunzip \
#  | tar x -C /usr/ \
#  && ln -s ${MAVEN_HOME} /usr/maven

## VERSIONS ##
ENV PATH=${PATH}:${JAVA_HOME}/bin

RUN ln -s ${JAVA_HOME_ACTUAL} ${JAVA_HOME} && \
    ls -al ${INSTALL_DIR} && \
    echo "JAVA_HOME=$JAVA_HOME" && \
    echo "PATH=${PATH}" && export JAVA_HOME=${JAVA_HOME} && export PATH=$PATH && \
#    mvn --version && \
    java -version

## VERSIONS ##
RUN echo "JAVA_HOME=$JAVA_HOME" && \
#    mvn --version && \
    java -version

#########################################
#### ---- Node from NODESOURCES ---- ####
#########################################
# Ref: https://github.com/nodesource/distributions
#ARG NODE_VERSION=${NODE_VERSION:-11}
#ENV NODE_VERSION=${NODE_VERSION}
#RUN apt-get update -y && \
#    apt-get install -y sudo curl git xz-utils && \
#    curl -sL https://deb.nodesource.com/setup_${NODE_VERSION}.x | bash - && \
#    apt-get install -y gcc g++ make && \
#    apt-get install -y nodejs && \
#    node -v && npm --version
#### --- Copy Entrypoint script in the container ---- ####
COPY ./docker-entrypoint.sh /

#### ------------------------
#### ---- user: Non-Root ----
#### ------------------------
ARG USER_NAME=${USER_NAME:-developer}
ENV USER_NAME=${USER_NAME}
ENV HOME=/home/${USER_NAME}

RUN export DISPLAY=${DISPLAY} && \
    useradd ${USER_NAME} && \
    export uid=${USER_ID} gid=${GROUP_ID} && \
    mkdir -p ${HOME} && \
    mkdir -p ${HOME}/workspace && \
    mkdir -p /etc/sudoers.d && \
    echo "${USER_NAME}:x:${USER_ID}:${GROUP_ID}:${USER_NAME},,,:${HOME}:/bin/bash" >> /etc/passwd && \
    echo "${USER_NAME}:x:${USER_ID}:" >> /etc/group && \
    echo "${USER_NAME} ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/${USER_NAME} && \
    chmod 0440 /etc/sudoers.d/${USER_NAME} && \
    chown ${USER_NAME}:${USER_NAME} -R ${HOME} && \
    apt-get clean all && \
    ls /usr/local/ 


WORKDIR ${HOME}
USER ${USER_NAME}
CMD ["/usr/bin/firefox"]
